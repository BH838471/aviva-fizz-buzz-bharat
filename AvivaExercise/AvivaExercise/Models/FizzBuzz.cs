﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
namespace AvivaExercise.Models
{
    //Step 3
    public class FizzBuzz
    {
        [Required(ErrorMessage = "Number is a Required Field")]
        [Display(Name = "Enter Number")]
        [Range(1, 1000,
           ErrorMessage = "Number must be between 1 and 1000")]
        public int number { get; set; }
        public string text { get; set; }

    }
}