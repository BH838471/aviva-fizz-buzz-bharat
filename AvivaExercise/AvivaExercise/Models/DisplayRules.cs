﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AvivaExercise.Models
{   
    //Step 2
    public class DisplayRules
    {
        public bool isDivisibleByFifteen(int num)
        {
            try
            {
                return (num % 15 == 0);
            }
            catch { return false; }
        }
        public bool isDivisibleByFive(int num)
        {
            try
            {
                return (num % 5 == 0);
            }
            catch { return false; }
        }
        public bool isDivisibleByThree(int num)
        {
            try
            {
                return (num % 3 == 0);
            }
            catch { return false; }
        }
        public FizzBuzz GetText(int num, string textOne, string textTwo)
        {
            try
            {
                FizzBuzz obj = new FizzBuzz();
                if (isDivisibleByFifteen(num))
                {
                    obj.number = num;
                    obj.text = textOne + " " + textTwo;
                    return obj;
                }
                else if (isDivisibleByFive(num))
                {
                    obj.number = num;
                    obj.text = textTwo;
                    return obj;

                }
                else if (isDivisibleByThree(num))
                {
                    obj.number = num;
                    obj.text = textOne;
                    return obj;

                }
                else
                {
                    obj.number = num;
                    obj.text = num.ToString();
                    return obj;

                }
            }
            catch { return null; }

        }
    }
}