﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AvivaExercise.Models;
using PagedList;

namespace AvivaExercise.Controllers
{
    //Step 4
    public class FizzBuzzController : Controller
    {
        
        const int pageSize = 20;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult paging(int? page, int? number)
        {
            int maxNumber = (number ?? 1);

            int pageNumber = (page ?? 1);
            ViewBag.Message = BindData(maxNumber).ToPagedList(pageNumber, pageSize);
            return View("DisplayFizzBuzz");

        }
        [HttpPost]
        public ActionResult Index(FizzBuzz obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int pageNumber = 1;
                    ViewBag.Message = BindData(obj.number).ToPagedList(pageNumber, pageSize);
                    return View("DisplayFizzBuzz");
                }
                return View();
            }
            catch { return View(); }
        }
        public List<FizzBuzz> BindData(int maxNumber)
        {
            try
            {
                DisplayRules rules = new DisplayRules();
                List<FizzBuzz> fizzBuzzList = new List<FizzBuzz>();
                string firstWord = "<span class='firstWord'>fizz</span>";
                string secondWord = "<span class='secondWord'>buzz</span>";
                if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                {
                    firstWord = "<span class='firstWord'>wizz</span>";
                    secondWord = "<span class='secondWord'>wuzz</span>";
                }
                for (int i = 1; i <= maxNumber; i++)
                {
                    fizzBuzzList.Add(rules.GetText(i, firstWord, secondWord));
                }
                return fizzBuzzList;
            }
            catch {
                return null;
            }
        }

    }
}
