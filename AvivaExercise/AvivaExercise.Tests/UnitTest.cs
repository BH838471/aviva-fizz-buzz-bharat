﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AvivaExercise.Models;

namespace AvivaExercise.Tests
{
    //Step 1
    [TestClass]
    public class UnitTestCases
    {
        
        [TestMethod]
        public void numIsDivisibleByThree()
        {
            DisplayRules objRules = new DisplayRules();
            bool blnStatus = objRules.isDivisibleByThree(3);
            Assert.IsTrue(blnStatus);
        }
        [TestMethod]
        public void numIsDivisibleByFive()
        {
            DisplayRules objRules = new DisplayRules();
            bool blnStatus = objRules.isDivisibleByFive(5);
            Assert.IsTrue(blnStatus);
        }
        [TestMethod]
        public void numIsDivisibleByFifteen()
        {
            DisplayRules objRules = new DisplayRules();
            bool blnStatus = objRules.isDivisibleByFifteen(15);
            Assert.IsTrue(blnStatus);
        }
    }
}
